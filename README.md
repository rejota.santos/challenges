## Instruções

# Instalação do projeto
Projeto Laravel v8.83.26

```bash
  composer install
  cp .env.example .env
  php artisan key:generate
  php artisan serve
```

# Resolução dos desafios
Os desafios ficarão disponíveis na lista de ISSUES e deverão possuir todas as informações necessárias para entendimento dos problemas a serem resolvidos. 

O Colaborador que quiser encarar o desafio, deverá criar um branch - a partir da ISSUE, resolver o desafio e abrir um MR para a branch master.

Não esquecer de marcar o responsável pela ISSUE como REVIEWER - isso vai ser importante para conseguirmos a aprovação quando o problema realmente for resolvido.

![Orientações Issues](image.png)

# Lista de desafios

Abaixo temos a relação dos desafios que já foram resolvidos

#### Desafio 1

Descrição do desafio
link da isseu
usuário responsável: @user1
colaboradores: @user2, @user3

## Dúvidas

Para suporte, mande um email para rejota.santos@gmail.com.
